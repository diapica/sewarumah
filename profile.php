<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/profile-yes.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <title>Profile</title>

     <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#upload").change(function(){
                readPath(this);
            }); 
        })

        function cancel(id_rent){
            res = confirm("Are you sure want to cancel this transaction ?");
            if(res==true){
                location.href = "process/cancel-renthouse.php?id_rent="+id_rent;
            }
        }

        function readPath(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile-photo').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
    <!-- PHP -->
    <?php 
      include "koneksi.php";
      
      if(!isset($_SESSION['id_user'])){
        header("location:login.php");
      }

      $query = "select * from tb_user where id=?";
      $users = $conn->prepare($query);
      $users->execute([$_SESSION['id_user']]);
      foreach($users as $key => $user){}
      $query = "select tb_rent.id as ID_RENT, tb_rent.*,tb_home.* from tb_rent inner join tb_home on tb_rent.id_home=tb_home.id where id_user_rent=?";
      $rents = $conn->prepare($query);
      $rents->execute([$_SESSION['id_user']]);
    ?>
  </head>
  <body>
  <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
        <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarReponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="index.php" class="nav-link text-center">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a href="rent_house_list.php" class="nav-link">RENT</a>
                    </li>
                    <li class="nav-item">
                        <a href="about_us.php" class="nav-link">ABOUT US</a>
                    </li>
                    <?php if(!isset($_SESSION['id_user'])){ ?>
                        <li class="nav-item">
                            <a href="login.php" class="nav-link">LOGIN</a>
                        </li>
                    <?php }else{ ?>
                        <li class="dropdown active active_nav">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="my-house-list.php">My House</a></li>
                                <li><a href="process/signout.php">Sign Out</a></li>
                            </ul>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </nav>
<div class="container-fluid">
    <div class="row col-sm-12">
        <div class="col-sm-6">
            <div class="kotak-profile mt-5 mb-5">
            <form enctype="multipart/form-data" method="POST" action="process/save_profile.php">
                <img class="rounded-circle mt-3" width="160px" height="180px" src="<?php echo $user['profile_picture'] ?>" id="profile-photo">
                <p><input type="file" class="tombol-profile" id="upload" name="upload"></p>
                <input type="hidden" name="prof_pict" value="<?php echo $user['profile_picture'] ?>">
                <hr>  
                <div class="bungkus text-left">
                    <div class="form-group">
                        <label class="judul-bold">Name :</label>
                        <input type="text" name="name" id="name" class="form-control" value="<?php echo $user['full_name']?>" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Phone Number :</label>
                        <input type="text" name="nohp" id="nohp" class="form-control" value="0<?php echo $user['phone_number']?>">
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Email :</label>
                        <input type="text" name="email" id="email" class="form-control" value="<?php echo $user['email']?>">
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Birth Date : (dd-mm-yyyy)</label>
                        <input type="text" name="birthdate" id="birthdate" class="form-control" value="<?php echo date("d-m-Y",strtotime($user['birth_date']))?>">
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Gender : (dd-mm-yyyy)</label>
                        <div class='radio'>
                            <label class="radio-inline"><input type="radio" name="gender" value="Male" <?php if($user['gender']=="Male") echo "checked" ?> >Male</label> &nbsp &nbsp
                            <label class="radio-inline"><input type="radio" name="gender" value="Female" <?php if($user['gender']=="Feale") echo "checked" ?> >Female</label> &nbsp &nbsp
                            <label class="radio-inline"><input type="radio" name="gender" value="Other"<?php if($user['gender']=="Other") echo "checked" ?> >Other</label> &nbsp &nbsp
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Bank :</label>
                        <input type="text" name="bank" id="bank" class="form-control" value="<?php echo $user['Bank_Acount'] ?>">
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Account Number :</label>
                        <input type="text" name="acc_number" id="ac_number" class="form-control" value="<?php echo $user['Account_number']?>">
                    </div>
                    <div class="tombol-bawah text-right">
                        <input type="submit" value="SAVE" class="tombol-save btn btn-primary">
                    </div>
                </div> 
            </form>   
            </div>
        </div>

        <div class="col-sm-6">
            <div class="kotak-kanan mt-5 table-responsive">
                <p class="judul-kanan text-center">TRANSACTION HISTORY</p>
                <table class="table">
                    <tr>
                        <th>NO</th>
                        <th>DATE</th>
                        <th>HOUSE NAME</th>
                        <th>STATUS</th>
                        <th colspan=2>ACTION</th>

                    </tr>
                    <?php if($rents->rowcount() == 0 ){?>
                        <tr>
                            <td colspan=5>No Transaction Record</td>
                        </tr>
                    <?php }else{
                        foreach($rents as $key => $rent){ ?>
                        <tr>
                            <td><?php echo $key+1 ?></td>
                            <td><?php echo date('d F Y',strtotime($rent['transaction_date'] ))?></td>
                            <td><?php echo $rent['house_name'] ?></td>
                            <td><span 
                            <?php if($rent['payment_status']=="Pending"){
                                        echo "class='pending'";
                                    }else if($rent['payment_status']=='Reject' || $rent['payment_status']=="Canceled" ){
                                        echo "class='reject'" ;
                                    }else{
                                        echo "class='accepted'";
                                    } ?>>
                                    <?php echo $rent['payment_status']?> </span></td>
                            <?php if($rent['payment_status']!="Canceled" ){ ?>
                                <td><a href="renthouse-detail.php?id_rent=<?php echo $rent['ID_RENT']?>"><input type="button" value="SEE DETAIL" class="btn btn-info btn-sm"></a></td>
                                <td><input type="button" value="CANCEL" id="cancel" class="btn btn-danger btn-sm" onclick="cancel('<?php echo $rent['ID_RENT']?>')"></td>
                            <?php }?>
                            
                            
                        </tr>
                    <?php } } ?>
                </table>
                
                <!-- <div class="text-center bungkus-tombol">
                    <button class="tombol text-center"><</button>
                    <button class="tombol text-center">1</button>
                    <button class="tombol text-center">></button>
                </div> -->
            </div>



        </div>

    </div>
</div>



<footer class="page-footer font-small pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
  
      <!-- Grid row -->
      <div class="row">
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
          <!-- Content -->
  
        </div>
        <!-- Grid column -->
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h6 class="text-uppercase footer-caption">Quick Links</h6>
  
            <ul class="list-unstyled">
                <li>
                    <a class="footercaption" href="index.php">HOME</a>
                </li>
                <li>
                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                </li>
                <li>
                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                </li>
            </ul>
            
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                <a class="footercaption" href="index.php"> SewaRumah.com</a>
            </div>
          <!-- Copyright -->
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h5 class="text-uppercase tulisan">contact us</h5>
  
            <ul class="list-unstyled">
                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                <img src="img/aset/email.png" alt="" class="imagefooter">
            </ul>
  
          </div>
          <!-- Grid column -->
  
      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->
 </body>
</html>