<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <link rel="stylesheet" href="css/signup.css">
    <title>Register - SewaRumah.com</title>

    <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      function cek_password(){
        var password = $('#password').val();
        var re_pass = $("#re-pass").val();
        if(password==re_pass){
          return true
        }else{
          alert('Password Tidak Cocok');
          return false;
        }
      }
    </script>

    <!-- PHP -->
    <?php 
      include "koneksi.php";
        if(isset($_SESSION['id_user'])){
          header("location:index.php");
        }
    ?>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
      <div class="container-fluid">
          <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarReponsive">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                      <a href="index.php" class="nav-link text-center">HOME</a>
                  </li>
                  <li class="nav-item">
                      <a href="rent_house_list.php" class="nav-link">RENT</a>
                  </li>
                  <li class="nav-item">
                      <a href="about_us.php" class="nav-link">ABOUT US</a>
                  </li>
                  <?php if(!isset($_SESSION['id'])){ ?>
                      <li class="nav-item  active active_nav">
                          <a href="login.php" class="nav-link">LOGIN</a>
                      </li>
                  <?php }else{ ?>
                      <li class="dropdown  active active_nav">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                              <li><a href="profile.php">Profile</a></li>
                              <li><a href="my-house-list.php">My House</a></li>
                              <li><a href="process/signout.php">Sign Out</a></li>
                          </ul>
                      </li>
                  <?php }?>
              </ul>
          </div>
      </div>
    </nav>
<!-- navbar end -->
<!-- login form -->
<div class="container-fluid">     
    <div class="row">
      <div class="col-sm-4"></div>
     
      <div class="col-sm-4 kotak">
        
        <form onsubmit="return cek_password()"  action="process/signup.php" method="POST">
          <p class="text-center"><img src="img/aset/logobirupolos.png" alt="" class="logo-sign"></p>
          <div class="tulisan"> <p class="text-center login font-weight-bold">SIGN UP</p></div>
         <p class="text-center "> <input class="asd img-fluid" type="text" name="fullname" id="fullname" placeholder="FULL NAME"></p>
         <p class="text-center "> <input class="asd img-fluid" type="text" name="phone" id="phone" placeholder="PHONE NUMBER"></p>
         <p class="text-center "> <input class="asd img-fluid" type="text" name="email" id="email" placeholder="EMAIL"></p>
         <p class="text-center "> <input class="asd img-fluid" type="password" name="password" id="password" placeholder="PASSWORD"></p>
         <p class="text-center "> <input class="asd img-fluid" type="password" name="re-pass" id="re-pass" placeholder="CONFIRM PASSWORD"></p>
         <div class="text-center">
                <label class="radio-inline jenkel">
                <input type="radio" name="gender" checked value="Male">MALE
                </label>
                <label class="radio-inline jenkel">
                <input type="radio" name="gender" value="Female">FEMALE
                </label>
                <label class="radio-inline jenkel">
                <input type="radio" name="gender" value="Other">OTHER
                  </label>
                </div>
                <p class="text-center account">By Clicking Sign Up, You Are Agreeing to the <a href="#">Terms of use</a> <br>
                    and Acknowledge the <a href="#">Privacy Policy</a> </p>
         <p class="text-center"><button class="sign-in asd img-fluid">SIGN UP</button></p>
         <p class="text-center account">HAVE AN ACCOUNT? <a href="login.php"><b>LOGIN HERE</b></a>
         </p>
        </form>
      </div>
      
      </div>
      <div class="col-sm-4"></div>
    </div>
  

  <!-- footer -->
  <footer class="page-footer font-small pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
  
      <!-- Grid row -->
      <div class="row">
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
          <!-- Content -->
  
        </div>
        <!-- Grid column -->
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h6 class="text-uppercase footer-caption">Quick Links</h6>
  
            <ul class="list-unstyled">
                <li>
                  <a class="footercaption" href="index.php">HOME</a>
                </li>
                <li>
                  <a class="footercaption" href="rent_house_list.php">RENT</a>
                </li>
                <li>
                  <a class="footercaption" href="about_us.php">ABOUT US</a>
                </li>
            </ul>
            
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                <a class="footercaption" href="index.php"> SewaRumah.com</a>
            </div>
          <!-- Copyright -->
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h5 class="text-uppercase tulisan">contact us</h5>
  
            <ul class="list-unstyled">
                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                <img src="img/aset/email.png" alt="" class="imagefooter">
            </ul>
  
          </div>
          <!-- Grid column -->
  
      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->
  </body>
</html>