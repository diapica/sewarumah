<?php 
    include '../koneksi.php';

    $email = $_POST['email'];
    $fullname = $_POST['name'];
    $birthdate = date('Y-m-d',strtotime($_POST['birthdate']));
    $gender = $_POST['gender'];
    $phone_number =$_POST['nohp'];
    $upload = $_FILES['upload'];
    if($upload['name']!=""){
        try{
            $path = "../img/profile_pict/";
            $img_name = $upload['tmp_name'];
            $profile_picture = $path.$_SESSION['id_user'].'-'.$upload['name'];
            move_uploaded_file($img_name,$profile_picture);
        }catch(Exception $e){
            echo "ERROR. GAGAL UPLOAD FOTO";
        }
    
        $path = "img/profile_pict/";
        $profile_picture = $path.$_SESSION['id_user'].'-'.$upload['name'];
    }else{
        $profile_picture = $_POST['prof_pict'];
    }

    echo $profile_picture;
    
    $acc_number = $_POST['acc_number'];
    $bank = $_POST['bank'];
    $id_user = $_SESSION['id_user'];

    $query = "update tb_user set full_name=?,email=?,birth_date=?,gender=?,phone_number=?,profile_picture=?,Account_number=?,Bank_Acount=? where id = ?";
    $arr = [$fullname,$email,$birthdate,$gender,$phone_number,$profile_picture,$acc_number,$bank,$id_user];
    $update = $conn->prepare($query);
    $update->execute($arr);
    $_SESSION['fullname'] = $fullname;
    header("location:../profile.php")

?>