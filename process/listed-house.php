<?php 
    include "../koneksi.php";

    $id_user = $_SESSION['id_user'];
    $house_name = $_POST['house_name'];
    $address = $_POST['address'];
    $price = $_POST['price'];
    $large = $_POST['large'];
    $bedroom = $_POST['bedroom'];
    $bathroom = $_POST['bathroom'];
    $garage = $_POST['garage'];
    $add_info = $_POST['add_info'];
    $upload = $_FILES['upload'];
    $button = $_POST['submit'];
    
    if($upload['name']!=""){
        try{
            $path = '../img/House Picture/';
            $filename = $upload['name'];
            $home_pict = $path.$_SESSION['id_user'].'-'.$filename;
            move_uploaded_file($upload['tmp_name'],$home_pict);
        }catch(Exception $e){
            echo "Gagal Pindahin Gambar";
        }

        $path = 'img/House Picture/';
        $filename = $upload['name'];
        $home_pict = $path.$_SESSION['id_user'].'-'.$filename;
    }else{
        $home_pict = $_POST['home_pict'];
    }

    if($button=="SUBMIT"){
        $query = "insert into tb_home(house_name,address,price,large,bedroom,bathroom,garage,add_information,house_pict,id_user) values (?,?,?,?,?,?,?,?,?,?)";
        $insert = $conn->prepare($query);
        $arr = [$house_name,$address,$price,$large,$bedroom,$bathroom,$garage,$add_info,$home_pict,$id_user];
        $insert->execute($arr);
    }else{
        $id_home = $_POST['id_home'];
        $query = "update tb_home set house_name=?, address=?, price=?,large=?,bedroom=?,bathroom=?,garage=?,add_information=?,house_pict=? where id = ?";
        $update = $conn->prepare($query);
        echo $query;
        $arr = [$house_name,$address,$price,$large,$bedroom,$bathroom,$garage,$add_info,$home_pict,$id_home];
        $update->execute($arr);
        echo $id_home;
    }
    
    header("location:../my-house-list.php")

    



?>