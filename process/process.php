<?php 
    include "../koneksi.php";

    $transaction_number = "SR-".date('Y-m')."-".rand(1,100000);
    $transaction_date = date("Y-m-d");
    $price = $_POST['price'];
    $durasi = $_POST['durasi'];
    $id_home = $_POST['home'];
    $id_user_rent = $_SESSION['id_user'];
    $total = $price * $durasi;
    $id_bank = $_POST['bank'];
    $bukti_transfer = $_FILES['upload'];
    $status = "Pending";

    try{
        $path = "../img/Bukti Transfer/";
        $img_name = $bukti_transfer['tmp_name'];
        $real_path = $path.$bukti_transfer['name'];
        move_uploaded_file($img_name,$real_path);
        
        $path = "img/Bukti Transfer/";
        $real_path = $path.$bukti_transfer['name'];
        
        $query = "insert into tb_rent (transaction_number,transaction_date,price,month,id_home,id_user_rent,total,id_bank,upload_payment,payment_status) values(?,?,?,?,?,?,?,?,?,?)";
        $sql = $conn->prepare($query);
        $arr = [$transaction_number,$transaction_date,$price,$durasi,$id_home,$id_user_rent,$total,$id_bank,$real_path,$status];
        $sql->execute($arr);

        header("location:../profile.php");

    }catch(Exception $e){
        echo "ga keupload";
    }


?>