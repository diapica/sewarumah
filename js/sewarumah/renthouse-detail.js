$(document).ready(function(){

    if($("#lama_pinjam").val()!="" && $("#lama_pinjam").val()!="0" ){
        $(".border-kiri").show();
    }else{
        $(".border-kiri").hide();
    }

    

    $("#lama_pinjam").keyup(function(){
        harga = $(this).attr("harga");
        lama_pinjam = $(this).val();
        total_harga = formatNumber(harga * lama_pinjam);
        $(".price").html("Rp."+total_harga);

        if($("#lama_pinjam").val()=="0"){
            $(".border-kiri").hide();
        }
        
    })

    $("#cancel").click(function(){
        window.history.back();
    })

    $("#submit").click(function(){
        if($("#lama_pinjam").val()!="0" && $("#lama_pinjam").val()!=""){
            $(".border-kiri").show();
        }
    })

    $("#upload").change(function(){
        readPath(this);
    });
})

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

function beforeSubmit(){
    var norek = $('#bank').val();
    var bukti_transfer = $("#upload").val();
    if(norek=="" || bukti_transfer==""){
        alert("Tolong Lengkapi data pada Halaman Konfirmasi");
        return false;
    }else{
        $("#durasi").val($("#lama_pinjam").val());;
        return true;
    }
}

function readPath(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#bukti_transfer').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
