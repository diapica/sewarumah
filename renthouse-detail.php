<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/renthouse-detail.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <title>Rent House Details</title>

    <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sewarumah/renthouse-detail.js"></script>
    <!-- PHP -->
    <?php 
      include "koneksi.php";
      if(!isset($_SESSION['id_user'])){
        header("location:login.php");
      }

      if(!isset($_GET['koderumah']) && !isset($_GET['id_rent'])){
        header("location:rent_house_list.php");
      }

      
      if(isset($_GET['id_rent'])){
        $id_rent = $_GET['id_rent'];
        $query = "select * from tb_rent inner join tb_home on tb_rent.id_home = tb_home.id  inner join tb_bank on tb_rent.id_bank = tb_bank.id  where tb_rent.id=$id_rent";
      }

      if(isset($_GET['koderumah'])){
        $koderumah = $_GET['koderumah'];
        $query = "select * from tb_home inner join tb_user on tb_home.id_user = tb_user.id where tb_home.id=$koderumah";
      }

      $rents = $conn->prepare($query);
      $rents->execute();

      $query = "select * from tb_bank";
      $banks = $conn->prepare($query);
      $banks->execute();

      foreach($rents as $key => $rent){}
    ?>
  </head>
  <body>
      <!-- navbar -->
      <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
              <div class="container-fluid">
                  <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarReponsive">
                      <ul class="navbar-nav ml-auto">
                          <li class="nav-item">
                              <a href="index.php" class="nav-link text-center">HOME</a>
                          </li>
                          <li class="nav-item active_nav active">
                              <a href="rent_house_list.php" class="nav-link">RENT</a>
                          </li>
                          <li class="nav-item">
                              <a href="about_us.php" class="nav-link">ABOUT US</a>
                          </li>
                          <?php if(!isset($_SESSION['id_user'])){ ?>
                              <li class="nav-item">
                                  <a href="login.php" class="nav-link">LOGIN</a>
                              </li>
                          <?php }else{ ?>
                              <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                      <li><a href="profile.php">Profile</a></li>
                                      <li><a href="my-house-list.php">My House</a></li>
                                      <li><a href="process/signout.php">Sign Out</a></li>
                                  </ul>
                              </li>
                          <?php }?>
                      </ul>
                  </div>
              </div>
      </nav>
    <!-- navbar end -->

    <p class="text-center tulisan">RENT HOUSE DETAIL</p>

    <div class="container-fluid kotakk">
        <div class="row">
          <div class="col-sm-6">
            <div class="col-sm-10">
                <p class="text-center judul"><?php echo $rent['house_name']?></p>
                <div class="bungkusfoto text-center">
                  <img src="<?php echo $rent['house_pict']?>" height=220  alt="" class="foto">
                </div>
                <p class="judul-bawah">Alamat</p>
                <p class="caption"><?php echo $rent['address']?><br>
                <p class="judul-bawah">Keterangan</p>
                <p class="caption">Luas : <?php echo $rent['large']?> m2</p>
                <p class="harga">Harga : Rp.<?php echo number_format($rent['price'],0,',','.')?>/Bulan</p>  
                  <?php if(isset($_GET['id_rent'])){ ?>
                        <p class="harga">Lama : <input type="text" name="lama_pinjam" disabled value="<?php echo $rent['month']?>" id="lama_pinjam" harga="<?php echo $rent['price'] ?>" placeholder="Bulan"> Bulan</p>
                  <?php }else{ ?>
                        <p class="harga">Lama : <input type="text" name="lama_pinjam" id="lama_pinjam" harga="<?php echo $rent['price'] ?>" placeholder="Bulan"> Bulan</p>
                  <?php } ?>

                  <?php if(isset($_GET['id_rent'])){ ?>
                        <p class="harga">Total : <span class="price">Rp.<?php echo number_format($rent['total'],0,',','.')?> </span> </p>   
                  <?php }else{ ?>
                    <p class="harga">Total : <span class="price">Rp.0 </span> </p>   
                  <?php } ?>
                
                <p class="text-left">
                  <?php if(isset($_GET['id_rent'])){ ?>
                        <input type="submit" value="Back" id="cancel" class="tombol-bawah btn btn-primary">
                  <?php }else{ ?>
                    <input type="submit" value="Cancel" id="cancel" class="tombol-bawah btn btn-primary">
                    <span class="tombol-kanan">
                    <input type="button" value="Submit" class="tombol-bawah btn btn-primary" id="submit">
                    </span>
                  <?php } ?>
                </p>
              </div>    
          </div>
          <div class="col-sm-6 border-kiri">
            <p class="text-center judul">Confirmation Page</p>
            
            <?php if(isset($_GET['id_rent'])){ ?>
              <form>
                <table>
                  <tr class="img-fluid">
                    <td><p class="harga">Nomor Rekening</p></td>
                    <td><p class="harga">:</p></td>
                    <td>       
                        <input type="text" disabled value="<?php echo $rent['bank'].'-'.$rent['account_number'] ?>">
                    </td>
                  </tr>
                  <tr class="img-fluid">
                    <td><p class="harga">Upload Bukti</p></td>
                    <td><p class="harga">:</p></td>
                    <td><input type="file" name="upload" disabled id="upload" class="img-fluid"></td>
                  </tr>
                  <tr class="img-fluid">
                    <td><p class="harga"></p></td>
                    <td><p class="harga"></p></td>
                    <td><br><img src="<?php echo $rent['upload_payment']?>" width=200 height=200 alt="" id="bukti_transfer"></td>
                  </tr>
                </table>
              </form>
            <?php }else{ ?>
              <form method="POST" enctype="multipart/form-data" id="form-process" onsubmit="return beforeSubmit()" action="process/process.php">
                <table>
                  <tr class="img-fluid">
                    <td><p class="harga">Nomor Rekening</p></td>
                    <td><p class="harga">:</p></td>
                    <td>       
                      <select name="bank" id="bank">
                          <option class="img-fluid" value="" disabled selected>-- Pilih Bank --</option>
                        <?php foreach($banks as $key=>$bank){ ?>
                          <option class="img-fluid" value="<?php echo $bank['id'] ?> "><?php echo $bank['bank'].'-'.$bank['account_number'] ?></option>
                        <?php } ?>
                        
                      </select>
                    </td>
                  </tr>
                  <tr class="img-fluid">
                    <td><p class="harga">Upload Bukti</p></td>
                    <td><p class="harga">:</p></td>
                    <td><input type="file" name="upload" id="upload" class="img-fluid"></td>
                  </tr>
                  <tr class="img-fluid">
                    <td><p class="harga"></p></td>
                    <td><p class="harga"></p></td>
                    <td><br><img src="img/aset/user.png" width=200 height=200 alt="" id="bukti_transfer"></td>
                  </tr>
                </table>

                <p class="text-right mt-5">
                  <input type="submit" name="proses" class="tombol-bawah btn btn-primary" id="proses" value="Process">
                </p>
                <input type="hidden" name='durasi' id="durasi">
                <input type="hidden" name='price' id="price" value="<?php echo $rent['price'] ?>">
                <input type="hidden" name='home' id="home" value="<?php echo $_GET['koderumah'] ?>">
              </form>
            <?php } ?>
          </div>  
        </div>
    </div>
<!-- footer -->
<    <footer class="page-footer font-small pt-4">

        <!-- Footer Links -->
        <div class="container-fluid text-center text-md-left">
      
          <!-- Grid row -->
          <div class="row">
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
              <!-- Content -->
      
            </div>
            <!-- Grid column -->
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h6 class="text-uppercase footer-caption">Quick Links</h6>
      
                <ul class="list-unstyled">
                  <li>
                    <a class="footercaption" href="index.php">HOME</a>
                  </li>
                  <li>
                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                  </li>
                  <li>
                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                  </li>
                </ul>
                
                <!-- Copyright -->
                <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                    <a class="footercaption" href="index.php"> SewaRumah.com</a>
                </div>
              <!-- Copyright -->
              </div>
              <!-- Grid column -->
      
              <!-- Grid column -->
              <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h5 class="text-uppercase tulisan">contact us</h5>
      
                <ul class="list-unstyled">
                    <img class="imagefooter" src="img/aset/facebook.png" alt="">
                    <img src="img/aset/instagram.png" alt="" class="imagefooter">
                    <img src="img/aset/twitter.png" alt="" class="imagefooter">
                    <img src="img/aset/email.png" alt="" class="imagefooter">
                </ul>
      
              </div>
              <!-- Grid column -->
      
          </div>
          <!-- Grid row -->
    
        </div>
        <!-- Footer Links -->
  </body>
</html>