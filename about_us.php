<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <link rel="stylesheet" href="css/aboutus.css">

     <!-- Javascript -->
     <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

     <!-- PHP -->
     <?php 
      include "koneksi.php";
      $query = "select * from tb_about_us";
      $abouts = $conn->prepare($query);
      $abouts->execute();
    ?>
    <title>About Us</title>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarReponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="index.php" class="nav-link text-center">HOME</a>
                        </li>
                        <li class="nav-item">
                            <a href="rent_house_list.php" class="nav-link">RENT</a>
                        </li>
                        <li class="nav-item active_nav active">
                            <a href="about_us.php" class="nav-link">ABOUT US</a>
                        </li>
                        <?php if(!isset($_SESSION['id_user'])){ ?>
                            <li class="nav-item">
                                <a href="login.php" class="nav-link">LOGIN</a>
                            </li>
                        <?php }else{ ?>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="profile.php">Profile</a></li>
                                    <li><a href="my-house-list.php">My House</a></li>
                                    <li><a href="process/signout.php">Sign Out</a></li>
                                </ul>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
    </nav>
  <!-- navbar end -->
  <!-- konten -->
    <p class="text-center about">ABOUT US</p>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Features -->
        <div class="row text-center">
          <?php foreach($abouts as $key => $about ){ ?>
          
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100 shadow p-3 mb-5 bg-white rounded">
            <img class="text-center rounded-circle profile" src="<?php echo $about['photo']?>" alt=""></img>
              <div class="card-body">
                <h4 class="card-title nama"><?php echo $about['name']?></h4>
                <p class="card-text desc"><?php echo $about['desc']?><p>
              </div>
            </div>
          </div>

          <?php } ?>
        </div>
        <!-- /.row -->
        <div col-sm-12>
          
            <div class="card h-100 shadow p-3 mb-5 bg-white rounded">
              <div class="card-body">
                <h4 class="card-title nama">Sumber : </h4>
                <p class="card-text desc"><a href = "https://www.w3schools.com/"  target="_blank" >https://www.w3schools.com/</a> <br><a href=" http://stackoverflow.com " target="_blank"> http://stackoverflow.com </a> <p>
              </div>
            </div>
          
        </div>
    </div>
        <!-- /.container -->



<!-- footer -->
<footer class="page-footer font-small pt-4 sticky-bottom">

        <!-- Footer Links -->
        <div class="container-fluid text-center text-md-left">
      
          <!-- Grid row -->
          <div class="row">
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
              <!-- Content -->
      
            </div>
            <!-- Grid column -->
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h6 class="text-uppercase footer-caption">Quick Links</h6>
      
                <ul class="list-unstyled">
                  <li>
                    <a class="footercaption" href="index.php">HOME</a>
                  </li>
                  <li>
                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                  </li>
                  <li>
                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                  </li>
                </ul>
                
                <!-- Copyright -->
                <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                    <a class="footercaption" href="index.php"> SewaRumah.com</a>
                </div>
              <!-- Copyright -->
              </div>
              <!-- Grid column -->
      
              <!-- Grid column -->
              <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h5 class="text-uppercase tulisan">contact us</h5>
      
                <ul class="list-unstyled">
                    <img class="imagefooter" src="img/aset/facebook.png" alt="">
                    <img src="img/aset/instagram.png" alt="" class="imagefooter">
                    <img src="img/aset/twitter.png" alt="" class="imagefooter">
                    <img src="img/aset/email.png" alt="" class="imagefooter">
                </ul>
      
              </div>
              <!-- Grid column -->
      
          </div>
          <!-- Grid row -->

        </div>
  </body>
</html>