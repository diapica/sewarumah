<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <link rel="stylesheet" href="css/login.css">
    
     <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- PHP -->
    <?php 
      include "koneksi.php";
      if(isset($_SESSION['id_user'])){
        header("location:index.php");
      }
    ?>
    <title>Login - SewaRumah.com</title>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
      <div class="container-fluid">
          <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarReponsive">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                      <a href="index.php" class="nav-link text-center">HOME</a>
                  </li>
                  <li class="nav-item">
                      <a href="rent_house_list.php" class="nav-link">RENT</a>
                  </li>
                  <li class="nav-item">
                      <a href="about_us.php" class="nav-link">ABOUT US</a>
                  </li>
                  <?php if(!isset($_SESSION['id'])){ ?>
                      <li class="nav-item active active_nav">
                          <a href="login.php" class="nav-link">LOGIN</a>
                      </li>
                  <?php }else{ ?>
                      <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                          <ul class="dropdown-menu active active_nav">
                              <li><a href="profile.php">Profile</a></li>
                              <li><a href="my-house-list.php">My House</a></li>
                              <li><a href="process/signout.php">Sign Out</a></li>
                          </ul>
                      </li>
                  <?php }?>
              </ul>
          </div>
      </div>
    </nav>
<!-- navbar end -->
<!-- login form -->
<div class="container-fluid">     
    <div class="row">
      <div class="col-sm-4"></div>
     
      <div class="col-sm-4 kotak">
        
        <form action="process/login.php" method="POST">
          <p class="text-center"><img src="img/aset/logobirupolos.png" alt="" class="logo-sign"></p>
          <div class="tulisan"> <p class="text-center login font-weight-bold">SIGN IN</p></div>
         <p class="text-center "> <input class="asd img-fluid" type="text" name="email" id="" placeholder="EMAIL"></p>
         <p class="text-center "> <input class="asd img-fluid" type="password" name="password" id="" placeholder="PASSWORD"></p>
         <p class="text-center"><button class="sign-in asd img-fluid">SIGN IN</button></p>
         <p class="text-center account">DON'T HAVE AN ACCOUNT? <a href="signup.php"><b>REGISTER HERE</b></a></p>
        </form>
      
      </div>
      
      </div>
      <div class="col-sm-4"></div>
    </div>
  

  <!-- footer -->
  <footer class="page-footer font-small pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
  
      <!-- Grid row -->
      <div class="row">
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
          <!-- Content -->
  
        </div>
        <!-- Grid column -->
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h6 class="text-uppercase footer-caption">Quick Links</h6>
  
            <ul class="list-unstyled">
                <li>
                  <a class="footercaption" href="index.php">HOME</a>
                </li>
                <li>
                  <a class="footercaption" href="rent_house_list.php">RENT</a>
                </li>
                <li>
                  <a class="footercaption" href="about_us.php">ABOUT US</a>
                </li>
            </ul>
            
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                <a class="footercaption" href="index.php"> SewaRumah.com</a>
            </div>
          <!-- Copyright -->
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h5 class="text-uppercase tulisan">contact us</h5>
  
            <ul class="list-unstyled">
                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                <img src="img/aset/email.png" alt="" class="imagefooter">
            </ul>
  
          </div>
          <!-- Grid column -->
  
      </div>
      <!-- Grid row -->

    </div>
  </body>
</html>