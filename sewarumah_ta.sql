-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2019 at 09:32 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sewarumah_ta`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_about_us`
--

CREATE TABLE `tb_about_us` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `desc` text NOT NULL,
  `photo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_about_us`
--

INSERT INTO `tb_about_us` (`id`, `name`, `desc`, `photo`) VALUES
(1, 'Sandro Owen', 'Bertanggung jawab pada bagian Back End dan sedikit membantu dibagian Front End', 'img/aboutus/114720.jpg'),
(2, 'Delwyn Marli', 'Bertanggung jawab pada bagian Front End', 'img/aboutus/delwyn.jpg'),
(3, 'Kevin', 'Bertanggung jawab untuk Design ', 'img/aboutus/kevin.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `id` int(10) NOT NULL,
  `bank` varchar(250) NOT NULL,
  `account_number` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`id`, `bank`, `account_number`) VALUES
(1, 'BCA', '1010202030'),
(2, 'BRI', '01234567890123');

-- --------------------------------------------------------

--
-- Table structure for table `tb_home`
--

CREATE TABLE `tb_home` (
  `id` int(10) NOT NULL,
  `house_name` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `price` float NOT NULL,
  `large` float NOT NULL,
  `bedroom` int(11) NOT NULL,
  `bathroom` int(11) NOT NULL,
  `garage` int(11) NOT NULL,
  `add_information` text NOT NULL,
  `house_pict` text NOT NULL,
  `id_user` int(10) NOT NULL,
  `approved` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_home`
--

INSERT INTO `tb_home` (`id`, `house_name`, `address`, `price`, `large`, `bedroom`, `bathroom`, `garage`, `add_information`, `house_pict`, `id_user`, `approved`) VALUES
(1, 'Allogio Gading Serpong ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 30000000, 300, 1, 1, 1, '-', 'img/house/rumah1.jpg', 1, 'no'),
(2, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 15000000, 300, 3, 1, 1, '-', 'img/house/rumah3.jpg', 1, 'yes'),
(3, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 20000000, 300, 1, 1, 1, '-', 'img/house/rumah2.jpg', 2, 'yes'),
(4, 'Allogio Gading Serpong', 'Jalan Gading Serpong Boulevard, Perumahan Allgio', 17000000, 300, 1, 1, 1, '-', 'img/house/rumah2.jpg', 2, 'yes'),
(5, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 10000000, 300, 1, 1, 1, '-', 'img/house/rumah1.jpg', 1, 'yes'),
(6, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 20000000, 300, 1, 1, 1, '-', 'img/house/rumah2.jpg', 2, 'yes'),
(7, 'Allogio Gading Serpong', 'Jalan Gading Serpong Boulevard, Perumahan Allgio', 17000000, 300, 1, 1, 1, '-', 'img/house/rumah2.jpg', 2, 'yes'),
(8, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 10000000, 300, 1, 1, 1, '-', 'img/house/rumah1.jpg', 1, 'yes'),
(9, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 15000000, 300, 1, 1, 1, '-', 'img/house/rumah3.jpg', 1, 'no'),
(10, 'Gading Serpong Boulevard ', 'Jalan Gading Serpong Boulevard, Komplek Citra Raya', 10000000, 300, 1, 1, 1, '-', 'img/house/rumah1.jpg', 1, 'no'),
(11, 'Kos Merahu', 'Jalan Gading Boulevard Depan UMN', 1200000, 100, 10, 4, 1, 'ga jadi tetst', 'img/House Picture/7-rumah2.jpg', 7, 'yes'),
(12, 'Kos Hijau', 'Jalan Gading Boulevard Depan UMN', 1200000, 100, 10, 4, 1, '', 'img/House Picture/7-rumah4.jpg', 7, 'yes'),
(13, 'Kos Hijau', 'Jalan Gading Boulevard Depan UMN', 1200000, 100, 10, 4, 1, '', 'img/House Picture/7-rumah4.jpg', 7, 'no'),
(14, 'baru', 'lama', 1000000, 100, 1, 2, 3, 'ga ada', 'img/House Picture/7-rumah2.jpg', 7, 'yes'),
(15, 'Allogio Dekostel', 'Jalan Allogio Barat 3 No 53', 1300000, 300, 9, 6, 0, '-dapur luas\r\n-ada kulkas', 'img/House Picture/7-rumah1.jpg', 7, 'no'),
(16, 'Rumah baru', 'Dimana aja', 1000000, 100, 1, 1, 0, 'ga ada', 'img/House Picture/1-rumah1.jpg', 1, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rent`
--

CREATE TABLE `tb_rent` (
  `id` int(10) NOT NULL,
  `transaction_number` varchar(20) NOT NULL,
  `transaction_date` date NOT NULL,
  `price` float NOT NULL,
  `month` int(11) NOT NULL,
  `id_home` int(10) NOT NULL,
  `id_user_rent` int(10) NOT NULL,
  `total` float NOT NULL,
  `id_bank` int(11) NOT NULL,
  `upload_payment` text NOT NULL,
  `payment_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rent`
--

INSERT INTO `tb_rent` (`id`, `transaction_number`, `transaction_date`, `price`, `month`, `id_home`, `id_user_rent`, `total`, `id_bank`, `upload_payment`, `payment_status`) VALUES
(1, 'SR-2019-05-71639', '2019-05-02', 15000000, 3, 2, 7, 45000000, 2, 'img/Bukti Transfer/slide2.jpg', 'Accepted'),
(4, 'SR-2019-05-25660', '2019-05-02', 15000000, 3, 2, 7, 45000000, 2, 'img/Bukti Transfer/slide2.jpg', 'Accepted'),
(5, 'SR-2019-05-98309', '2019-05-04', 17000000, 1, 7, 7, 17000000, 2, 'img/Bukti Transfer/Dahyun-TWICE7-650x975.jpg', 'Pending'),
(6, 'SR-2019-05-39388', '2019-05-04', 1300000, 3, 15, 7, 3900000, 2, 'img/Bukti Transfer/Dahyun-TWICE7-650x975.jpg', 'Rejected'),
(7, 'SR-2019-05-61475', '2019-05-06', 1300000, 3, 15, 1, 3900000, 1, 'img/Bukti Transfer/rumah4.jpg', 'Canceled');

-- --------------------------------------------------------

--
-- Table structure for table `tb_slideshow`
--

CREATE TABLE `tb_slideshow` (
  `id` int(10) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slideshow`
--

INSERT INTO `tb_slideshow` (`id`, `image`) VALUES
(1, 'img/img/login.jpg'),
(2, 'img/img/slide1.jpg'),
(3, 'img/slideshow/rumah2.jpg'),
(4, 'img/slideshow/rumah4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_status`
--

CREATE TABLE `tb_status` (
  `id` int(10) NOT NULL,
  `status` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_status`
--

INSERT INTO `tb_status` (`id`, `status`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL,
  `email` varchar(250) NOT NULL,
  `full_name` varchar(250) NOT NULL,
  `password` varchar(20) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` enum('Male','Female','Other','') NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `id_status` int(10) NOT NULL,
  `profile_picture` text NOT NULL,
  `Account_number` int(20) NOT NULL,
  `Bank_Acount` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `full_name`, `password`, `birth_date`, `gender`, `phone_number`, `id_status`, `profile_picture`, `Account_number`, `Bank_Acount`) VALUES
(1, 'sandro@student.umn.ac.id', 'Sandro Owen', '123123123', '1970-01-01', 'Male', '0081234567899', 2, 'img/profile_pict/1-Dahyun-TWICE7-650x975.jpg', 0, ''),
(2, 'budi@student.umn.ac.id', 'budinamaku', '123123123', '0000-00-00', 'Male', '822331122', 2, 'img/aset/user.png', 0, ''),
(7, 'admin', 'Sandro', 'admin', '1999-07-18', 'Male', '811223344', 1, 'img/profile_pict/7-Dahyun-TWICE7-650x975.jpg', 123123123, 'BCA'),
(8, 'delwyn_ganteng@student.ui.ac.id', 'Delwyn Marli', 'ganteng69', '0000-00-00', 'Male', '086969696969', 2, 'img/aset/user.png', 0, ''),
(9, 'lightg49@gmail.com', '123', '123', '0000-00-00', 'Male', '123', 2, 'img/aset/user.png', 0, ''),
(10, 'delwyn@student.umn.ac.id', 'Delwyn Marli', '12345', '0000-00-00', 'Male', '012345678', 2, 'img/aset/user.png', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_about_us`
--
ALTER TABLE `tb_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_home`
--
ALTER TABLE `tb_home`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_rent`
--
ALTER TABLE `tb_rent`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_number` (`transaction_number`),
  ADD KEY `tb_rent_ibfk_1` (`id_user_rent`),
  ADD KEY `id_home` (`id_home`),
  ADD KEY `id_bank` (`id_bank`);

--
-- Indexes for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_status`
--
ALTER TABLE `tb_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id_status` (`id_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_about_us`
--
ALTER TABLE `tb_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_home`
--
ALTER TABLE `tb_home`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_rent`
--
ALTER TABLE `tb_rent`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_status`
--
ALTER TABLE `tb_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_home`
--
ALTER TABLE `tb_home`
  ADD CONSTRAINT `tb_home_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id`);

--
-- Constraints for table `tb_rent`
--
ALTER TABLE `tb_rent`
  ADD CONSTRAINT `tb_rent_ibfk_1` FOREIGN KEY (`id_user_rent`) REFERENCES `tb_user` (`id`),
  ADD CONSTRAINT `tb_rent_ibfk_2` FOREIGN KEY (`id_home`) REFERENCES `tb_home` (`id`),
  ADD CONSTRAINT `tb_rent_ibfk_3` FOREIGN KEY (`id_bank`) REFERENCES `tb_bank` (`id`);

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `tb_status` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
