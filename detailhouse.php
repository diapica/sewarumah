<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/detailhouse.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <title>House-Detail</title>

    <!-- JavaScript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <?php 
        include 'koneksi.php';
        if(!isset($_GET['koderumah'])){
            header("location:rent_house_list.php");
        }

        $koderumah = $_GET['koderumah'];
        $query = "select * from tb_home inner join tb_user on tb_home.id_user = tb_user.id where tb_home.id=$koderumah";
        $rents = $conn->prepare($query);
        $rents->execute();

    ?>
  </head>

  <body>
     <!-- navbar -->
        <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarReponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="index.php" class="nav-link text-center">HOME</a>
                            </li>
                            <li class="nav-item active_nav active">
                                <a href="rent_house_list.php" class="nav-link">RENT</a>
                            </li>
                            <li class="nav-item">
                                <a href="about_us.php" class="nav-link">ABOUT US</a>
                            </li>
                            <?php if(!isset($_SESSION['id_user'])){ ?>
                                <li class="nav-item">
                                    <a href="login.php" class="nav-link">LOGIN</a>
                                </li>
                            <?php }else{ ?>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="profile.php">Profile</a></li>
                                        <li><a href="my-house-list.php">My House</a></li>
                                        <li><a href="process/signout.php">Sign Out</a></li>
                                    </ul>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
        </nav>
  <!-- navbar end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9">
        
            <div class="judul-atas">
                <p class="judul-kiri">
                                
                    <?php foreach ($rents as $key => $rent) { echo $rent['house_name']?> <?php }?>

                    <em>
                    <span class="judul-kanan">Rp.<?php echo number_format($rent['price'],0,',','.')?>/ Bulan</span></em>
                </p>
            </div>

            <div class="container-fluid">
                <div class="foto-keterangan row">
                    <div class="foto">
                        <img class="img-fluid foto-rumah" src="<?php echo $rent['house_pict'] ?>" alt="">
                    </div>
                    <div class="desc img-fluid">
                        <p class="judul-ini">Alamat</p>
                        <p class="keterangan"><?php echo $rent['address'] ?></p>

                        <p class="judul-ini">Keterangan</p>
                        <p class="keterangan">Luas : <?php echo $rent['large']?> m2</p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <p class="judul-ini">Fasilitas</p>
                    <div class="col-sm-4 float-left">
                        <div class="col-sm-12">
                            <img width=60 height=40 src="img/aset/tempat_tidur.png">
                            <span class="keterangan2"><?php echo $rent['bedroom'] ?> Kamar Tidur</span>
                        </div>
                    </div>
                    <div class="col-sm-4 float-left">
                        <div class="col-sm-12">
                            <img width=60 height=40 src="img/aset/kamar_mandi.png">
                            <span class="keterangan2"><?php echo $rent['bathroom'] ?> Kamar Mandi</span>
                        </div>
                    </div>
                    <div class="col-sm-4 float-left">
                        <div class="col-sm-12">
                            <img width=60 height=40 src="img/aset/garasi.png">
                            <span class="keterangan2"><?php echo $rent['garage'] ?> Garasi</span>
                        </div>
                    </div>
                </div>

                </br>
                </br>
                <div class="col-sm-12">
                    <p class="judul-ini">Keterangan Tambahan</p>
                    <div class="col-sm-12 float-left">
                        <div class="col-sm-12">
                            <span class="keterangan2">
                            <?php if($rent['add_information']!=""){
                                echo $rent['add_information'];
                                }else{
                                    echo "No Additional Information Here";
                                } 
                                ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 mt-5">
                    <div class="col-sm-6 float-left">
                        <a href="rent_house_list.php"><input type="submit" value="Back To List" class="tombol-bawah"></a>
                    </div>
                    <?php if(isset($_SESSION['id_user'])){ ?>
                        <div class="col-sm-6 float-right text-right">
                            <a href="renthouse-detail.php?koderumah=<?php echo $koderumah?>"><input type="submit" value="Rent" class="tombol-bawah"></a>
                        </div>
                    <?php }else{ ?>
                        <div class="col-sm-6 float-right text-right">
                            <a href="login.php"><input type="submit" value="LOGIN" class="tombol-bawah"></a>
                        </div>
                    <?php } ?>
                    
                </div>
                
            </div>
        </div>


        <div class="col-sm-3">
            <div class="kotak-kanan">
                <div class="card h-100 shadow p-3 mb-5 bg-white rounded">
                    <p class="owner">Owner</p>
                    <div class="text-center">
                            <img class="rounded-circle profile" src="<?php echo $rent['profile_picture'] ?> " alt="">
                    </div>
                    <div class="nama"><?php echo $rent['full_name'] ?></div>
                    <div class="imgtelpon">
                        <i class="fa fa-phone fa-2x icon"></i> <span class="info">+62-<?php echo number_format($rent['phone_number'],0,',','-')?></span>
                        <br>
                        <i class="fa fa-envelope fa-2x icon"></i> <span class="info"> <?php echo $rent['email'] ?></span>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>


<!-- footer -->
<footer class="page-footer font-small pt-4">

        <!-- Footer Links -->
        <div class="container-fluid text-center text-md-left">
      
          <!-- Grid row -->
          <div class="row">
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
              <!-- Content -->
      
            </div>
            <!-- Grid column -->
      
            <!-- Grid column -->
            <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h6 class="text-uppercase footer-caption">Quick Links</h6>
      
                <ul class="list-unstyled">
                  <li>
                    <a class="footercaption" href="index.php">HOME</a>
                  </li>
                  <li>
                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                  </li>
                  <li>
                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                  </li>
                </ul>
                
                <!-- Copyright -->
                <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                    <a class="footercaption" href="index.php"> SewaRumah.com</a>
                </div>
              <!-- Copyright -->
              </div>
              <!-- Grid column -->
      
              <!-- Grid column -->
              <div class="col-sm-4 tulisan">
      
                <!-- Links -->
                <h5 class="text-uppercase tulisan">contact us</h5>
      
                <ul class="list-unstyled">
                    <img class="imagefooter" src="img/aset/facebook.png" alt="">
                    <img src="img/aset/instagram.png" alt="" class="imagefooter">
                    <img src="img/aset/twitter.png" alt="" class="imagefooter">
                    <img src="img/aset/email.png" alt="" class="imagefooter">
                </ul>
      
              </div>
              <!-- Grid column -->
      
          </div>
          <!-- Grid row -->

        </div>
        <!-- Footer Links -->
  </body>
</html>