<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/listednew-house.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <title>Listed New House</title>

    <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#upload").change(function(){
                readPath(this);
            }); 
        })

        function readPath(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#house_photo').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
    <!-- PHP -->
    <?php 
      include "koneksi.php";
      if(!isset($_SESSION['id_user'])){
          header("location:login.php");
      }

      if(isset($_GET['id_home'])){
          $query = "select * from tb_home where id = ?";
          $homes = $conn->prepare($query);
          $homes->execute([$_GET['id_home']]);
          foreach($homes as $key => $home){};
      }
    ?>

  </head>
  <body>
<!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
        <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarReponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="index.php" class="nav-link text-center">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a href="rent_house_list.php" class="nav-link">RENT</a>
                    </li>
                    <li class="nav-item">
                        <a href="about_us.php" class="nav-link">ABOUT US</a>
                    </li>
                    <?php if(!isset($_SESSION['id_user'])){ ?>
                        <li class="nav-item">
                            <a href="login.php" class="nav-link">LOGIN</a>
                        </li>
                    <?php }else{ ?>
                        <li class="dropdown active active_nav">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="my-house-list.php">My House</a></li>
                                <li><a href="process/signout.php">Sign Out</a></li>
                            </ul>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </nav>
<!-- navbar end -->

<!-- konten -->
<div class="container-fluid mt-2">
<p class="listed-new"><a href="my-house-list.php">Back</a>
    <span style="float:right;">+ Listed New House</span>
</p>

</div>
<?php if(isset($_GET['id_home'])){ ?>
    <div class="container-fluid">
        <div class="row col-sm-12">
            <div class="col-sm-6">
                <form method="POST" action="process/listed-house.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="judul-bold">House Name :</label>
                        <input type="text" name="house_name" id="house_name" class="form-control" placeholder="House Name" value="<?php echo $home['house_name'] ?>" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Address :</label>
                        <textarea class="form-control" name="address" id="address" placeholder="Address"><?php echo $home['address'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Price / Month  : </label>
                        <input type="text" name="price" id="price" class="form-control" placeholder="Price / Month" value="<?php echo $home['price'] ?>">
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">House Large (M <sup>2</sup>) :</label>
                        <input type="text" name="large" id="large" class="form-control" placeholder="House Large" value="<?php echo $home['large'] ?>">
                    </div>
                    <div>
                        <label class="judul-bold">Facility :</label>
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bedroom :</label>
                        <input type="text" name="bedroom" id="bedroom" class="form-control" placeholder="Bedroom" value="<?php echo $home['bedroom'] ?>" >
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bathroom :</label>
                        <input type="text" name="bathroom" id="bathroom" class="form-control" placeholder="Bathroom" value="<?php echo $home['bathroom'] ?>">
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Garage :</label>
                        <input type="text" name="garage" id="garage" class="form-control" placeholder="Garage" value="<?php echo $home['garage'] ?>" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Additional Information :</label>
                        <textarea class="form-control" name="add_info" id="add_info" placeholder="Additional Information"><?php echo $home['add_information'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">House Picture :</label>
                        <input type="file" name="upload" id="upload">
                        <input type="hidden" name="home_pict" value="<?php echo $home['house_pict']?>">
                    </div>
                    <div class="form-group  text-right">
                        <input type="hidden" name="id_home" value="<?php echo $home['id']?>">
                        <input type="submit" name="submit" id="submit" value="Update" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <div class="mb-5"> House Picture : </div>
                    <img style="height:200px;" id="house_photo" src="<?php echo $home['house_pict'] ?>" >          
            </div>
        </div>
    </div>
<?php }else{ ?> 
    <div class="container-fluid">
        <div class="row col-sm-12">
            <div class="col-sm-6">
                <form method="POST" action="process/listed-house.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="judul-bold">House Name :</label>
                        <input type="text" name="house_name" id="house_name" class="form-control" placeholder="House Name" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Address :</label>
                        <textarea class="form-control" name="address" id="address" placeholder="Address"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Price / Month  : </label>
                        <input type="text" name="price" id="price" class="form-control" placeholder="Price / Month" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">House Large (M <sup>2</sup>) :</label>
                        <input type="text" name="large" id="large" class="form-control" placeholder="House Large" >
                    </div>
                    <div>
                        <label class="judul-bold">Facility :</label>
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bedroom :</label>
                        <input type="text" name="bedroom" id="bedroom" class="form-control" placeholder="Bedroom" >
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bathroom :</label>
                        <input type="text" name="bathroom" id="bathroom" class="form-control" placeholder="Bathroom" >
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Garage :</label>
                        <input type="text" name="garage" id="garage" class="form-control" placeholder="Garage" >
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Additional Information :</label>
                        <textarea class="form-control" name="add_info" id="add_info" placeholder="Additional Information"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">House Picture :</label>
                        <input type="file" name="upload" id="upload">
                        <input type="hidden" name="home_pict">
                    </div>
                    <div class="form-group  text-right">
                        <input type="submit" name="submit" id="submit" value="SUBMIT" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <div class="mb-5"> House Picture : </div>
                    <img style="height:200px;" id="house_photo" src="img/aset/upload.png" >          
            </div>
        </div>
    </div>
<?php } ?>





<!-- footer -->
<footer class="page-footer font-small pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
  
      <!-- Grid row -->
      <div class="row">
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
          <!-- Content -->
  
        </div>
        <!-- Grid column -->
  
        <!-- Grid column -->
        <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h6 class="text-uppercase footer-caption">Quick Links</h6>
  
            <ul class="list-unstyled">
                <li>
                    <a class="footercaption" href="index.php">HOME</a>
                </li>
                <li>
                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                </li>
                <li>
                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                </li>
            </ul>
            
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                <a class="footercaption" href="index.php"> SewaRumah.com</a>
            </div>
          <!-- Copyright -->
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-sm-4 tulisan">
  
            <!-- Links -->
            <h5 class="text-uppercase tulisan">contact us</h5>
  
            <ul class="list-unstyled">
                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                <img src="img/aset/email.png" alt="" class="imagefooter">
            </ul>
  
          </div>
          <!-- Grid column -->
  
      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->
  </footer>
  </body>
</html>