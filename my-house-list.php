<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/myhouse.css">
    <link rel="stylesheet" href="css/header_footer.css">
    <title>My House</title>

    <!-- Javascript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <?php 
      include "koneksi.php";
      if(!isset($_SESSION['id_user'])){
          header("location:login.php");
      }
      $id_user = $_SESSION['id_user'];
      $query = "select tb_home.id as HomeId , tb_home.*, tb_user.* from tb_home inner join tb_user on tb_home.id_user=tb_user.id where id_user = ?";
      $homes = $conn->prepare($query);
      $homes->execute([$id_user]);
    ?>

  </head>
  <body>
       <!-- navbar -->
       <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
        <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarReponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="index.php" class="nav-link text-center">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a href="rent_house_list.php" class="nav-link">RENT</a>
                    </li>
                    <li class="nav-item">
                        <a href="about_us.php" class="nav-link">ABOUT US</a>
                    </li>
                    <?php if(!isset($_SESSION['id_user'])){ ?>
                        <li class="nav-item">
                            <a href="login.php" class="nav-link">LOGIN</a>
                        </li>
                    <?php }else{ ?>
                        <li class="dropdown active active_nav">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="my-house-list.php">My House</a></li>
                                <li><a href="process/signout.php">Sign Out</a></li>
                            </ul>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </nav>
    <!-- navbar end -->
    <div class="container-fluid px-5">
        <p style="text-align:left;" class="rent">
            My House
            <span style="float:right;"><a href="listednew-house.php">+ Listed New House</a></span>
        </p>
    </div>
    <!-- konten isi -->

    <div class="container">
        <div class="row ">
            <div class="col-sm-12 margin">
                <?php foreach($homes as $key => $home){ ?>
                    <div class="col-sm-3 bagi3  kotak_sewa">
                        <div class="col-sm-12 kotak shadow-lg p-3 mb-5 bg-white rounded">
                            <div class="kotak_atas2">
                                    <img height="130px" src="<?php echo $home['house_pict']?>"></img>
                            </div>
                            <div class="kotak_bungkus_bawah">
                                <div class="kotak_bawah2">
                                    <?php echo $home['house_name'] ?> <br>
                                    <p><?php echo $home['address'] ?></p>
                                    <strong><p class="text-center f24">Rp. <?php echo number_format($home['price'],0,',','.')?> / Bulan</p> </strong>
                                    <p>Can Be Rent :
                                        <?php if($home['approved']=="no") { ?>
                                            <label style="color:red">Not Approved</label>
                                        <?php }else if($home['approved']=="yes"){ ?>
                                            <label style="color:green">Approved</label>
                                        <?php } ?>
                                    </p>
                                </div>
                                <div class="kotak_bawah3">
                                        <div class="imgtelpon captionanon text-left text-nowrap"><img src="img/aset/telfon.png" alt="">&nbsp; <?php echo $home['full_name'] ?> <br>&emsp;&emsp;&emsp;  +62<?php echo number_format($home['phone_number'],0,',','-')?></div>
                                        <a href="listednew-house.php?id_home=<?php echo $home['HomeId']?>"><div class="detail text-right">Details ></div></a>
                                </div>
                            </div>  
                        </div>  
                    </div>              
                <?php } ?>
            </div>
        </div>            
    </div>

        <footer class="page-footer font-small pt-4">

                <!-- Footer Links -->
                <div class="container-fluid text-center text-md-left">
              
                  <!-- Grid row -->
                  <div class="row">
              
                    <!-- Grid column -->
                    <div class="col-sm-4 tulisan">
              
                      <!-- Content -->
              
                    </div>
                    <!-- Grid column -->
              
                    <!-- Grid column -->
                    <div class="col-sm-4 tulisan">
              
                        <!-- Links -->
                        <h6 class="text-uppercase footer-caption">Quick Links</h6>
              
                        <ul class="list-unstyled">
                            <li>
                                <a class="footercaption" href="index.php">HOME</a>
                            </li>
                            <li>
                                <a class="footercaption" href="rent_house_list.php">RENT</a>
                            </li>
                            <li>
                                <a class="footercaption" href="about_us.php">ABOUT US</a>
                            </li>
                        </ul>
                        
                        <!-- Copyright -->
                        <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                            <a class="footercaption" href="index.php"> SewaRumah.com</a>
                        </div>
                      <!-- Copyright -->
                      </div>
                      <!-- Grid column -->
              
                      <!-- Grid column -->
                      <div class="col-sm-4 tulisan">
              
                        <!-- Links -->
                        <h5 class="text-uppercase tulisan">contact us</h5>
              
                        <ul class="list-unstyled">
                            <img class="imagefooter" src="img/aset/facebook.png" alt="">
                            <img src="img/aset/instagram.png" alt="" class="imagefooter">
                            <img src="img/aset/twitter.png" alt="" class="imagefooter">
                            <img src="img/aset/email.png" alt="" class="imagefooter">
                        </ul>
              
                      </div>
                      <!-- Grid column -->
              
                  </div>
                  <!-- Grid row -->
            
                </div>
                <!-- Footer Links -->
              </footer>
              <!-- footer end -->
  </body>
</html>