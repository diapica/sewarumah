<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">
    <link rel="stylesheet" href="../../css/Admin/admin-master_data.css">
    <title>Admin Panel-Master Data</title>

     <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>
    <script>
        function edit(id,status){
            $('#status').val(status)
            $('#id').val(id);
            $('#submit').val("Update");
        }
    </script>

     <!--PHP  -->
    <?php 
      include "../../koneksi.php";
      
      if(!isset($_SESSION['id_user'])){
        header("location:../../login.php");
      }
      if($_SESSION['status_user']==2){
        header("location:../../");
      }

      $query = "select * from tb_status";
      $statuses = $conn->prepare($query);
      $statuses->execute();
    ?>
  </head>
  <body>
    
<!-- navbar -->
<!-- <div class="col-sm-2"> -->
    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="aboutus.php">About Us</a>
          <a href="bank.php">Bank</a>
          <a href="slideshow.php">SlideShow</a>
          <a href="status.php">Status</a>
          <a href="user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>            
<!-- navbar end -->




  <div class="content">
    <div class="col-sm-12 judul-atas">
      <label class="">Form Status</label>
      <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
    </div>  
    <div class="col-sm-12 mt-3">
      <div class="col-sm-12">
        <form enctype="multipart/form-data" method="POST" action="../../process/admin/master/status/save.php">
          <div class="form-group">
            <label>Status : </label>
            <input type="text" class="form-control" id="status" name="status">
          </div>
          <div class="form-group col-sm-2 float-right ">
            <input type="submit" class="form-control btn btn-primary btn-save" value="Save" id="submit" name="submit">
          </div>
          <input type="hidden" class="form-control" id="id" name="id">
        </form>
      </div>
      <table class="table">
        <tr>
            <th>NO</th>
            <th>Status</th>
            <th>ACTION</th>
        </tr>
        <?php foreach($statuses as $key => $status){ ?>
        <tr>
            <td><?php echo $key+1?></td>
            <td><?php echo $status['status']?></td>
            <td>
              <input type="button" class="btn btn-info" onclick="edit('<?php echo $status['id'] ?>','<?php echo $status['status'] ?>')" value="Edit">
              <a href="../../process/admin/master/status/delete.php?id=<?php echo $status['id']?>"><input type="button" class="btn btn-danger" value="Delete"></a>
            </td>
        </tr>
        <?php } ?>
      </table>
    </div>
  </div> 
</body>
</html>