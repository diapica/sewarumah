<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">
    <link rel="stylesheet" href="../../css/Admin/admin-master_data.css">
    <title>Admin Panel-Master Data</title>

     <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>
    <script>
        $(document).ready(function(){
            $("#upload").change(function(){
                readPath(this);
            }); 
        })

        function readPath(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshow').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        
        function edit(id,image){
            $('#slideshow').attr('src', '../../'+image);
            $('#id_slideshow').val(id);
            $('#submit').val("Update");
        }
    </script>

     <!--PHP  -->
    <?php 
      include "../../koneksi.php";
      
      if(!isset($_SESSION['id_user'])){
        header("location:../../login.php");
      }
      if($_SESSION['status_user']==2){
        header("location:../../");
      }

      $query = "select * from tb_slideshow";
      $slideshows = $conn->prepare($query);
      $slideshows->execute();
    ?>
  </head>
  <body>
    
<!-- navbar -->
<!-- <div class="col-sm-2"> -->
    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="aboutus.php">About Us</a>
          <a href="bank.php">Bank</a>
          <a href="slideshow.php">SlideShow</a>
          <a href="status.php">Status</a>
          <a href="user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>            
<!-- navbar end -->




  <div class="content">
    <div class="col-sm-12 judul-atas">
      <label class="">Form Slideshow</label>
      <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
    </div>  
    <div class="col-sm-12 mt-3">
      <div class="col-sm-6 float-left">
        <form enctype="multipart/form-data" method="POST" action="../../process/admin/master/slideshow/save.php">
          <div class="form-group">
            <label>SlideShow : </label>
            <input type="file" class="form-control" id="upload" name="upload">
          </div>
          <div class="form-group col-sm-3 float-right ">
            <input type="submit" class="form-control btn btn-primary btn-save" value="Save" id="submit" name="submit">
          </div>
          <input type="hidden" class="form-control" id="id_slideshow" name="id_slideshow">
        </form>
      </div>
      <div class="col-sm-6 float-right">
        <form>
          <div class="form-group">
            <img src="../../img/aset/upload.png" height="200" width='500' id="slideshow">
          </div>
        </form>
      </div>  
      <table class="table">
        <tr>
            <th>NO</th>
            <th>Image</th>
            <th>ACTION</th>
        </tr>
        <?php foreach($slideshows as $key => $slideshow){ ?>
        <tr>
            <td><?php echo $key+1?></td>
            <td><img src="../../<?php echo $slideshow['image']?>" width=150 height=60></td>
            <td>
              <input type="button" class="btn btn-info" onclick="edit('<?php echo $slideshow['id'] ?>','<?php echo $slideshow['image'] ?>')" value="Edit">
              <a href="../../process/admin/master/slideshow/delete.php?id=<?php echo $slideshow['id']?>"><input type="button" class="btn btn-danger" value="Delete"></a>
            </td>
        </tr>
        <?php } ?>
      </table>
    </div>
  </div> 
</body>
</html>