<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">
    <link rel="stylesheet" href="../../css/Admin/admin-master_data.css">
    <title>Admin Panel-Master Data</title>

     <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>
    <script>
        $(document).ready(function(){
            $('#submit').hide();
        })

       
        function edit(id,image,name,email,status){
            $('#photo').attr('src', '../../'+image);
            $('#id').val(id);
            $('#name').val(name);
            $('#email').val(email);
            $('#real_path').val(image);
            $("#status").val(status);
            $('#submit').show();
            $('#submit').val("Update");
        }
    </script>

     <!--PHP  -->
    <?php 
      include "../../koneksi.php";
      
      if(!isset($_SESSION['id_user'])){
        header("location:../../login.php");
      }
      if($_SESSION['status_user']==2){
        header("location:../../");
      }

      $query = "select tb_user.*,tb_status.*,tb_user.id as ID_USER from tb_user inner join tb_status on tb_user.id_status=tb_status.id";
      $users = $conn->prepare($query);
      $users->execute();

      $query = "select * from tb_status";
      $statuses = $conn->prepare($query);
      $statuses->execute();
    ?>
  </head>
  <body>
    
<!-- navbar -->
<!-- <div class="col-sm-2"> -->
    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="aboutus.php">About Us</a>
          <a href="bank.php">Bank</a>
          <a href="slideshow.php">SlideShow</a>
          <a href="status.php">Status</a>
          <a href="user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>            
<!-- navbar end -->




  <div class="content">
    <div class="col-sm-12 judul-atas">
      <label class="">Form User</label>
      <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
    </div>  
    <div class="col-sm-12 mt-3">
      <div class="col-sm-8 float-left">
        <form enctype="multipart/form-data" method="POST" action="../../process/admin/master/user/save.php">
          <div class="form-group">
            <label>Full Name : </label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          <div class="form-group">
            <label>E-Mail :  </label>
            <input type="text" class="form-control" id="email" name="email">
          </div>
          <div class="form-group">
            <label>Status : </label>
            <select clas="form-control" id='status' name='status'>
                <option value="0" disabled>---Status---</option>
                <?php foreach($statuses as  $key=> $status){?>
                    <option value="<?php echo $status['id']?>"><?php echo $status['status']?></option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group col-sm-3 float-right ">
            <input type="submit" class="form-control btn btn-primary btn-save" value="Save" id="submit" name="submit">
          </div>
          <input type="hidden" class="form-control" id="id" name="id">
          <input type="hidden" class="form-control" id="real_path" name="real_path">
        </form>
      </div>
      <div class="col-sm-4 float-right">
        <form>
          <div class="form-group mt-5">
            <img src="../../img/aset/upload.png" height="200" width='200' id="photo">
          </div>
        </form>
      </div>  
      <table class="table">
        <tr>
            <th>NO</th>
            <th>Full Name</th>
            <th>E-Mail</th>
            <th>Phone Number</th>
            <th>Status</th>
            <th>ACTION</th>
        </tr>
        <?php foreach($users as $key => $user){ ?>
        <tr>
            <td><?php echo $key+1?></td>
            <td><?php echo $user['full_name']?></td>
            <td><?php echo $user['email']?></td>
            <td><?php echo $user['phone_number']?></td>
            <td><?php echo $user['status']?></td>
            <td><img src="../../<?php echo $user['profile_picture']?>" width=100 height=100></td>
            <td>
              <input type="button" class="btn btn-info" onclick="edit('<?php echo $user['ID_USER'] ?>','<?php echo $user['profile_picture'] ?>','<?php echo $user['full_name']?>','<?php echo $user['email']?>','<?php echo $user['id_status']?>')" value="Edit">
              <a href="../../process/admin/master/user/delete.php?id=<?php echo $user['ID_USER']?>"><input type="button" class="btn btn-danger" value="Delete"></a>
            </td>
        </tr>
        <?php } ?>
      </table>
    </div>
  </div> 
</body>
</html>