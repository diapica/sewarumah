<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/Admin/header-admin.css">
    <link rel="stylesheet" href="../css/Admin/admin-home.css">
    <title>Admin Panel- Home</title>

     <!-- Javascript -->
    <script src="../js/jquery.3.2.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/Admin/home-header.js"></script>
    
    <!-- PHP -->
    <?php 
      include "../koneksi.php";
      
      if(!isset($_SESSION['id_user'])){
        header("location:../login.php");
      }
      if($_SESSION['status_user']==2){
        header("location:../");
      }

      $query = "select * from tb_user where id_status !=1";
      $user = $conn->prepare($query);
      $user->execute();

      $query = "select * from tb_rent where payment_status=?";
      $payment = $conn->prepare($query);
      $payment->execute(['Pending']);

      $query = "select * from tb_home where approved=?";
      $home = $conn->prepare($query);
      $home->execute(['yes']);
    ?>

  </head>
  <body>
    
<!-- navbar -->
<!-- <div class="col-sm-2"> -->
    <div class="sidenav">
        <a class="navbar-brand asd" href="admin-home.php"><img src="../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="master/aboutus.php">About Us</a>
          <a href="master/bank.php">Bank</a>
          <a href="master/slideshow.php">SlideShow</a>
          <a href="master/status.php">Status</a>
          <a href="master/user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="payment/history.php">Payment History</a>
          <a href="payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="rent/request">Listed Rent House Request</a>
          <a href="rent/list-house">Listed Rent House List</a>
        </div>
        <a href="../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>
    <!-- </div> -->
    <!-- navbar end -->




<div class="content">
        <p class="text-right judul-atas">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</p>
        <div class="row">
        
        <div class="col-sm-4">
            <a href="master/user.php">
                <div class="kotak text-center shadow p-3 mb-5 bg-white rounded">
                    <p><img class="img-fluid" src="../img/aset/users.png" alt=""></p>
                    <p class="angka-biru text-center"><?php echo $user->rowcount() ?></p>
                    <p class="angka-bawah text-center">Registered User</p>
                </div>
            </a>
        </div>
        <div class="col-sm-4">
            <a href="payment/request.php">
                <div class="kotak text-center shadow p-3 mb-5 bg-white rounded">
                    <p><img class="img-fluid" src="../img/aset/payment_pending.png" alt=""></p>
                    <p class="angka-kuning text-center"><?php echo $payment->rowcount() ?></p>
                    <p class="angka-bawah text-center">Pending Payment</p>
                </div>
            </a>
        </div>

        <div class="col-sm-4">
            <a href="rent/list-house.php">
                <div class="kotak text-center shadow p-3 mb-5 bg-white rounded">
                    <p><img class="img-fluid" src="../img/aset/rent-home.png" alt=""></p>
                    <p class="angka-biru text-center"><?php echo $home->rowcount() ?></p>
                    <p class="angka-bawah text-center">Listed House Rented</p>
                </div>
            </a>
        </div>
    </div>




</div> 


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>