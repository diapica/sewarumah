<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">

    <title>Admin Panel- Payment History</title>
    
    <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>

    <!-- PHP -->
    <?php 
        include "../../koneksi.php";
        
        if(!isset($_SESSION['id_user'])){
        header("location:../login.php");
        }
        if($_SESSION['status_user']==2){
        header("location:../");
        }
        if(!isset($_GET['id'])){
            header("location:history.php");
        }

        $query = "select tb_rent.id as ID_RENT, tb_rent.*, tb_home.*, tb_user.* from tb_rent inner join tb_home on tb_rent.id_home = tb_home.id inner join tb_user on tb_rent.id_user_rent = tb_user.id where tb_rent.id = ?";
        $rents = $conn->prepare($query);
        $rents->execute([$_GET['id']]);

        foreach($rents as $key => $rent)
    ?>
  </head>
  <body>
    
<!-- navbar -->

    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../master/aboutus.php">About Us</a>
          <a href="../master/bank.php">Bank</a>
          <a href="../master/slideshow.php">SlideShow</a>
          <a href="../master/status.php">Status</a>
          <a href="../master/user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>          
<!-- navbar end -->




    <div class="content">
        <div class="col-sm-12 judul-atas">
            <label class="">Payment Detail - Invoice No : <?php echo $rent['transaction_number']?> </label>
            <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
        </div> 
        <div class="col-sm-12 mt-3">
            <div class="col-sm-6 float-left">
                <form enctype="multipart/form-data" method="POST" action="../../process/admin/payment/update.php">
                    <div class="form-group">
                        <label>Full Name : </label>
                        <input type="text" class="form-control" id="f_name" name="f_name" disabled value="<?php echo $rent['full_name']?>">
                    </div>
                    <div class="form-group">
                        <label>House Name : </label>
                        <input type="text" class="form-control" id="h_name" name="h_name" disabled value=<?php echo $rent['house_name']?>>
                    </div>
                    <div class="form-group">
                        <label>Month : </label>
                        <input type="text" class="form-control" id="month" name="month" disabled value=<?php echo $rent['month']?>>
                    </div>
                    <div class="form-group">
                        <label>Price / Month : </label>
                        <input type="text" class="form-control" id="price" name="price" disabled value=<?php echo $rent['price']?>>
                    </div>
                    <div class="form-group">
                        <label>Total Price : </label>
                        <input type="text" class="form-control" id="total" name="total" disabled value=<?php echo $rent['total']?>>
                    </div>
                    <div class="form-group">
                        <label>Payment Status : </label>
                        <select clas="form-control" id='status' name='status'>
                                <option value="0" disabled>--- Payment Status</option>
                                <option value="Accepted" <?php if($rent['payment_status']=='Accepted'){echo "selected"; }?>>Accepted</option>
                                <option value="Pending"  <?php if($rent['payment_status']=='Pending'){echo "selected"; }?>>Pending</option>
                                <option value="Rejected" <?php if($rent['payment_status']=='Rejected'){echo "selected"; }?>>Rejected</option>
                                <option value="Canceled" <?php if($rent['payment_status']=='Canceled'){echo "selected"; }?>>Canceled</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 float-left ">
                        <a href="javascript:history.back()"><input type="button" class="form-control btn btn-danger" value="Back" id="back" name="back"></a>
                    </div>
                    <div class="form-group col-sm-3 float-right ">
                        <input type="submit" class="form-control btn btn-primary btn-save" value="Update" id="submit" name="submit">
                    </div>
                    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $rent['ID_RENT'] ?>">
                </form>
            </div>
            <div class="col-sm-6 float-right mt-4">
                <form>
                <div class="form-group">
                    <img src="../../<?php echo $rent['upload_payment']?>" height="400" width='500' id="slideshow">
                </div>
                </form>
            </div> 
        </div>
    </div> 
  </body>
</html>