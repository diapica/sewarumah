<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">

    <title>Admin Panel- Payment History</title>
    
    <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>

    <!-- PHP -->
    <?php 
        include "../../koneksi.php";
        if(!isset($_SESSION['id_user'])){
            header("location:../login.php");
        }
        if($_SESSION['status_user']==2){
            header("location:../");
        }
        $query = "select tb_rent.id as ID_RENT, tb_rent.*, tb_home.* from tb_rent inner join tb_home on tb_rent.id_home = tb_home.id where payment_status ='Pending'";
        $rents = $conn->prepare($query);
        $rents->execute();
    ?>
  </head>
  <body>
    
<!-- navbar -->

    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../master/aboutus.php">About Us</a>
          <a href="../master/bank.php">Bank</a>
          <a href="../master/slideshow.php">SlideShow</a>
          <a href="../master/status.php">Status</a>
          <a href="../master/user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>          
<!-- navbar end -->




    <div class="content">
        <div class="col-sm-12 judul-atas">
        <label class="">Payment Request</label>
        <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
        </div> 
        <div class="col-sm-12 mt-3">

        <table class="table">
            <tr>
                <th>No</th>
                <th>Date</th>
                <th>House Name</th>
                <th>Status</th>
                <th>ACTION</th>
            </tr>
            <?php foreach($rents as $key => $rent){ ?>
            <tr>
                <td><?php echo $key+1?></td>
                <td><?php echo date('d M Y',strtotime($rent['transaction_date']))?></td>
                <td><?php echo $rent['house_name'] ?></td>
                <td style='color:rgb(255, 174, 0)'>
                    <b><?php echo $rent['payment_status'] ?></b>
                </td>
                <td>
                <a href="detail.php?id=<?php echo $rent['ID_RENT']?>"><input type="button" class="btn btn-info" value="See Detail"></a>
                </td>
            </tr>
            <?php } ?>
        </table>
        </div>
    </div> 
  </body>
</html>