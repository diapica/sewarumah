<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">

    <title>Admin Panel- Payment History</title>
    
    <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>

    <!-- PHP -->
    <?php 
        include "../../koneksi.php";
        
        if(!isset($_SESSION['id_user'])){
        header("location:../login.php");
        }
        if($_SESSION['status_user']==2){
        header("location:../");
        }
        if(!isset($_GET['id'])){
            header("location:history.php");
        }

        $query = "select * from tb_home where id = ?";
        $homes = $conn->prepare($query);
        $homes->execute([$_GET['id']]);

        foreach($homes as $key => $home)
    ?>
  </head>
  <body>
    
<!-- navbar -->

    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../master/aboutus.php">About Us</a>
          <a href="../master/bank.php">Bank</a>
          <a href="../master/slideshow.php">SlideShow</a>
          <a href="../master/status.php">Status</a>
          <a href="../master/user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../rent/list-house.php">Listed Rent House List</a>
          <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>          
<!-- navbar end -->




    <div class="content">
        <div class="col-sm-12 judul-atas">
            <label class="">Home Rent Detail </label>
            <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
        </div> 
        <div class="col-sm-12 mt-3">
            <div class="col-sm-6 float-left">
                <form enctype="multipart/form-data" method="POST" action="../../process/admin/rent/update.php">
                    <div class="form-group">
                        <label>House Name : </label>
                        <input type="text" class="form-control" id="h_name" name="h_name" disabled value="<?php echo $home['house_name']?>">
                    </div>
                    <div class="form-group">
                        <label>Large m<sup>2</sup> : </label>
                        <input type="text" class="form-control" id="large" name="large" disabled value="<?php echo $home['large']?>">
                    </div>
                    <div class="form-group">
                        <label>Price / Month : </label>
                        <input type="text" class="form-control" id="price" name="price" disabled value="<?php echo $home['price']?>">
                    </div>
                    <div class="form-group">
                        <label>Address : </label>
                        <textarea class="form-control" id="address" name="address" disabled><?php echo $home['address']?></textarea>
                    </div>
                    <div>
                        <label class="judul-bold">Facility :</label>
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bedroom :</label>
                        <input type="text" name="bedroom" id="bedroom" class="form-control" placeholder="Bedroom" value="<?php echo $home['bedroom']?>" disabled>
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Bathroom :</label>
                        <input type="text" name="bathroom" id="bathroom" class="form-control" placeholder="Bathroom" value="<?php echo $home['bathroom']?>" disabled>
                    </div>
                    <div class="form-group col-sm-4 float-left">
                        <label class="judul-bold">Garage :</label>
                        <input type="text" name="garage" id="garage" class="form-control" placeholder="Garage" value="<?php echo $home['garage']?>" disabled>
                    </div>
                    <div class="form-group">
                        <label class="judul-bold">Additional Information :</label>
                        <textarea class="form-control" name="add_info" id="add_info" placeholder="Additional Information" disabled><?php echo $home['add_information']?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Approved : </label>
                        <select clas="form-control" id='status' name='status'>
                                <option value="0" disabled>--- Approval---</option>
                                <option value="yes" <?php if($home['approved']=='yes'){echo "selected"; }?>>Yes</option>
                                <option value="no"  <?php if($home['approved']=='no'){echo "selected"; }?>>No</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 float-left ">
                        <a href="javascript:history.back()"><input type="button" class="form-control btn btn-danger" value="Back" id="back" name="back"></a>
                    </div>
                    <div class="form-group col-sm-3 float-right ">
                        <input type="submit" class="form-control btn btn-primary btn-save" value="Update" id="submit" name="submit">
                    </div>
                    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $home['id'] ?>">
                </form>
            </div>
            <div class="col-sm-6 float-right mt-4">
                <form>
                <div class="form-group">
                    <img src="../../<?php echo $home['house_pict']?>" height="400" width='500' id="slideshow">
                </div>
                </form>
            </div> 
        </div>
    </div> 
  </body>
</html>