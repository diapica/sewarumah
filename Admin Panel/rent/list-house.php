<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/Admin/header-admin.css">

    <title>Admin Panel- Payment History</title>
    
    <!-- Javascript -->
    <script src="../../js/jquery.3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/Admin/home-header.js"></script>

    <!-- PHP -->
    <?php 
        include "../../koneksi.php";
        if(!isset($_SESSION['id_user'])){
            header("location:../login.php");
        }
        if($_SESSION['status_user']==2){
            header("location:../");
        }
        
        if(!isset($_SESSION['id_user'])){
            header("location:../login.php");
        }
        if($_SESSION['status_user']==2){
            header("location:../");
        }
        $query = "select tb_home.id as ID_HOME, tb_home.*, tb_user.* from tb_home inner join tb_user on tb_home.id_user = tb_user.id where approved = 'yes'";
        $homes = $conn->prepare($query);
        $homes->execute();
    ?>
  </head>
  <body>
    
<!-- navbar -->

    <div class="sidenav">
        <a class="navbar-brand asd" href="../"><img src="../../img/aset/logoo.png" alt=""></a>
        <!-- dropdown master data -->
        <button class="dropdown-btn"><b>MASTER DATA</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../master/aboutus.php">About Us</a>
          <a href="../master/bank.php">Bank</a>
          <a href="../master/slideshow.php">SlideShow</a>
          <a href="../master/status.php">Status</a>
          <a href="../master/user.php">User</a>
        </div>

        <!-- dropdown payment -->
        <button class="dropdown-btn"><b>PAYMENT</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
          <a href="../payment/history.php">Payment History</a>
          <a href="../payment/request.php">Payment Request</a>
        </div>
        <!-- drop down listed rent house -->
        <button class="dropdown-btn"><b>LISTED RENT HOUSE</b>
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="dropdown-container">
            <a href="../rent/list-house.php">Listed Rent House List</a>  
            <a href="../rent/request.php">Listed Rent House Request</a>
        </div>
        <a href="../../process/signout.php"><button class="dropdown-btn"><b>Sign Out</b></button></a>
    </div>          
<!-- navbar end -->




    <div class="content">
        <div class="col-sm-12 judul-atas">
        <label class="">List Rent House</label>
        <label class="float-right">Welcome Back, <?php echo $_SESSION['fullname'] ?> !</label>
        </div> 
        <div class="col-sm-12 mt-3">

        <table class="table">
            <tr>
                <th>No</th>
                <th>House Name</th>
                <th>Owner</th>
                <th>Approved</th>
                <th>ACTION</th>
            </tr>
            <?php foreach($homes as $key => $home){ ?>
            <tr>
                <td><?php echo $key+1?></td>
                <td><?php echo $home['house_name'] ?></td>
                <td><?php echo $home['full_name'] ?></td>
                <td style='color:green'>
                    <b><?php echo strtoupper($home['approved']) ?></b>
                </td>
                <td>
                <a href="detail.php?id=<?php echo $home['ID_HOME']?>"><input type="button" class="btn btn-info" value="See Detail"></a>
                </td>
            </tr>
            <?php } ?>
        </table>
        </div>
    </div> 
  </body>
</html>