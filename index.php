<html>
    <header>
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/header_footer.css">
        <link rel="stylesheet" href="css/index.css">

          <!-- JavaScript -->
        <script src="js/jquery.3.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>
            function seemore(){
                location.href = "rent_house_list.php";
            }
            
        </script>

        <!-- PHP -->
        <?php 
            include "koneksi.php";
            $query = "select * from tb_slideshow";
            $slideshows = $conn->prepare($query);
            $slideshows->execute();

            $query = "select * from tb_home inner join tb_user on tb_home.id_user = tb_user.id where approved='yes' limit 3";
            $homes = $conn->prepare($query);
            $homes->execute();
        ?>
    </header>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarReponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active_nav active_nav active">
                                <a href="index.php" class="nav-link text-center">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a href="rent_house_list.php" class="nav-link">RENT</a>
                            </li>
                            <li class="nav-item">
                                <a href="about_us.php" class="nav-link">ABOUT US</a>
                            </li>
                            <?php if(!isset($_SESSION['id_user'])){ ?>
                                <li class="nav-item">
                                    <a href="login.php" class="nav-link">LOGIN</a>
                                </li>
                            <?php }else{ ?>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="profile.php">Profile</a></li>
                                        <li><a href="my-house-list.php">My House</a></li>
                                        <li><a href="process/signout.php">Sign Out</a></li>
                                    </ul>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
        </nav>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <?php foreach($slideshows as $key => $slideshow){ 
                        if ($key==0) {
                            echo '<div class="carousel-item active">';
                        }else{
                            echo '<div class="carousel-item">';
                        }
                ?>
                    <img class="d-block w-100 fotoslide" height="600px" src="<?php echo $slideshow['image'] ?>" alt="First slide">
                    </div>

                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="warnabungkusicon">
            <div class="container">
                    <div class="row">
                        <div class="col-sm-12 margin">
                            <div class="col-sm-4 bagi3">
                                <div class="col-sm-12 kotak">
                                    <div class="kotak_atas">
                                        <img src="img/aset/Icon Rumah.png"></img>
                                    </div>
                                    <div class="kotak_bawah">
                                        Look For Rental House That Match <br> Your Wants and Budget
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 bagi3">
                                <div class="col-sm-12 kotak">
                                    <div class="kotak_atas">
                                            <img src="img/aset/Safety.png"></img>
                                    </div>
                                    <div class="kotak_bawah">
                                        Trusted Landers <br> We Can Help You Find A Rent House
                                        </div>
                                </div>
                            </div>
                            <div class="col-sm-4 bagi3">
                                    <div class="col-sm-12 kotak">
                                        <div class="kotak_atas">
                                                <img src="img/aset/rent-home.png"></img>
                                        </div>
                                        <div class="kotak_bawah">
                                            Decide To Rent Your House <br> With Our Website
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 margin">
                    <?php foreach ($homes as $key => $home){ ?>
                    <div class="col-sm-4 bagi3  kotak_sewa">
                        <div class="col-sm-12 kotak shadow-lg p-3 mb-5 bg-white rounded">
                            <div class="kotak_atas2">
                                    <img src="<?php echo $home['house_pict'] ?>"></img>
                            </div>
                            <div class="kotak_bungkus_bawah">
                                    <div class="kotak_bawah2">
                                        <?php echo $home['house_name'] ?> <br>
                                        <p><?php echo $home['address'] ?></p>
                                        <strong><p class="text-center f24">Rp.<?php echo number_format($home['price'],0,',','.') ?> / Bulan</p> </strong>
                                    </div>
                                    <div class="kotak_bawah3">
                                            <div class="imgtelpon captionanon text-left text-nowrap"><img src="img/aset/telfon.png" alt="">&nbsp; <?php echo $home['full_name']?> <br>&emsp;&emsp;&emsp;  +62<?php echo number_format($home['phone_number'],0,',','-') ?></div>
                                            <div class="detail text-right"><a href="detailhouse.php?koderumah=<?php echo $home['id'] ?>">Details > </a></div>
                                    </div>
                            </div>  
                        </div>  
                    </div>
                    <?php } ?>  
                </div>     
            </div>
        </div>
        <div class="seemore text-center"><button class="seemorebtn btn-lg" onclick="seemore()">SEE MORE</button></div>
        <footer class="page-footer font-small pt-4">

                    <!-- Footer Links -->
                    <div class="container-fluid text-center text-md-left">
                  
                      <!-- Grid row -->
                      <div class="row">
                  
                        <!-- Grid column -->
                        <div class="col-sm-4 tulisan">
                  
                          <!-- Content -->
                  
                        </div>
                        <!-- Grid column -->
                  
                        <!-- Grid column -->
                        <div class="col-sm-4 tulisan">
                  
                            <!-- Links -->
                            <h6 class="text-uppercase footer-caption">Quick Links</h6>
                  
                            <ul class="list-unstyled">
                                <li>
                                    <a class="footercaption" href="index.php">HOME</a>
                                </li>
                                <li>
                                    <a class="footercaption" href="rent_house_list.php">RENT</a>
                                </li>
                                <li>
                                    <a class="footercaption" href="about_us.php">ABOUT US</a>
                                </li>
                            </ul>
                            
                            <!-- Copyright -->
                            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                                <a class="footercaption" href="index.php"> SewaRumah.com</a>
                            </div>
                          <!-- Copyright -->
                          </div>
                          <!-- Grid column -->
                  
                          <!-- Grid column -->
                          <div class="col-sm-4 tulisan">
                  
                            <!-- Links -->
                            <h5 class="text-uppercase tulisan">contact us</h5>
                  
                            <ul class="list-unstyled">
                                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                                <img src="img/aset/email.png" alt="" class="imagefooter">
                            </ul>
                  
                          </div>
                          <!-- Grid column -->
                  
                      </div>
                      <!-- Grid row -->

                    </div>
                    <!-- Footer Links -->
                  
                    
                  
                  </footer>
    </body>
</html>