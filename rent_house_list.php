<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/listed.css">
    <link rel="stylesheet" href="css/header_footer.css">

     <!-- JavaScript -->
    <script src="js/jquery.3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
            function search_(){
                    var cur_url = window.location.href;
                    var name = $('#name').val();
                    var price = $('#price').val();
                    var location = $('#location').val();
                    
                    var sp_url = cur_url.split("&split");
                    var url = sp_url[0]+'&split&name='+name+'&price='+price+'&location='+location;

                    var ot_url = cur_url.split("?page=");
                    var row_page = ot_url[1].split("&");
                    if(row_page[0]!=1 && row_page[2]!="name"){
                        var url = ot_url[0]+'?page=1&split&name='+name+'&price='+price+'&location='+location;
                    }
                    window.location.href = url;
            }

            function search(){
                var cur_url = window.location.href;
                    var name = $('#name').val();
                    var price = $('#price').val();
                    var location = $('#location').val();
                    var url = cur_url+'?page=1&split&name='+name+'&price='+price+'&location='+location;
                    window.location.href = url;
            }
    </script>
    
    <!-- PHP -->
    <?php 
        include 'koneksi.php';
        $limit = 9;        
        
        if(isset($_GET['page'])){
                $page = $_GET['page'];
                $start = ($page-1)*$limit;
                $previous = $page-1;
                $next = $_GET['page']+1;
        }else{
                $page = 1;
                $start = 0;
                $next = 2;
        }
        $kondisi = "approved='yes'";

        $url = "";

        if(isset($_GET['name'])){
                $url = "&name=".$_GET['name']."&price=".$_GET['price']."&location=".$_GET['location'];
               
                if($_GET['name']!=""){
                        $kondisi = $kondisi."and house_name like '%".$_GET['name']."%'";
                }
                if($_GET['price']!=""){
                        $kondisi = $kondisi."and price=".$_GET['price'];
                }
                if($_GET['location']!=""){
                        $kondisi = $kondisi."and address like '%".$_GET['location']."%'";
                }
        }
        
        $query = "select tb_home.id as ID_HOME ,tb_home.*,tb_user.* from tb_home inner join tb_user on tb_home.id_user = tb_user.id where $kondisi limit $start,$limit";
        $homes = $conn->prepare($query);
        $homes->execute();

        //Untuk Pagination
        $query = "select * from tb_home where $kondisi";
        $sql = $conn->prepare($query);
        $sql->execute();

        $jumlah_data = $sql->rowcount();
        $page_total = ceil($jumlah_data/$limit);
        
    ?>

    <title>Our Rent House</title>
  </head>
  <body>
    <!-- navbar -->
        <nav class="navbar navbar-expand-md navbar-dark  sticky-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php"><img src="img/aset/logoo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarReponsive">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarReponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="index.php" class="nav-link text-center">HOME</a>
                            </li>
                            <li class="nav-item active_nav active">
                                <a href="rent_house_list.php" class="nav-link">RENT</a>
                            </li>
                            <li class="nav-item">
                                <a href="about_us.php" class="nav-link">ABOUT US</a>
                            </li>
                            <?php if(!isset($_SESSION['id_user'])){ ?>
                                <li class="nav-item">
                                    <a href="login.php" class="nav-link">LOGIN</a>
                                </li>
                            <?php }else{ ?>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">HI <?php echo strtoupper($_SESSION['fullname']) ?> <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="profile.php">Profile</a></li>
                                        <li><a href="my-house-list.php">My House</a></li>
                                        <li><a href="process/signout.php">Sign Out</a></li>
                                    </ul>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
        </nav>
    <!-- navbar end -->
    <p class="text-center rent">OUR RENT HOUSE</p>

    <div class="container">
            <div class="row ">
                    <div class="col-sm-9 margin">
                            <?php 
                            if($jumlah_data ==0){
                                    echo '<p class="text-center tulisan2">No House Found</p>';
                            }else{
                                foreach ($homes as $key=>$home) { ?>
                                        <div class="col-sm-4 bagi3  kotak_sewa">                                               
                                                <div class="col-sm-12 kotak shadow-lg p-3 mb-5 bg-white rounded">
                                                        <div class="kotak_atas2">
                                                                <img height="130px" src="<?php echo $home['house_pict']?>"></img>
                                                        </div>
                                                        <div class="kotak_bungkus_bawah">
                                                                <div class="kotak_bawah2">
                                                                <?php echo $home['house_name']?> <br>
                                                                <p><?php echo $home['address'] ?></p>
                                                                <strong><p class="text-center f20">Rp.<?php echo number_format($home['price'],0,',','.')?> / Bulan</p> </strong>
                                                                </div>
                                                                <div class="kotak_bawah3">
                                                                        <div class="imgtelpon captionanon text-left text-nowrap"><img src="img/aset/telfon.png" alt="">&nbsp; <?php echo $home['full_name'] ?> <br>&emsp;&emsp;&emsp;  +62-<?php echo number_format($home['phone_number'],0,',','-')?></div>
                                                                        <div class="detail text-right"><a href="detailhouse.php?koderumah=<?php echo $home['ID_HOME'] ?>">Details > </a></div>
                                                                </div>
                                                        </div>    
                                                </div>  
                                        </div>
                                <?php } ?> 
                                <div class="paging text-center">
                                        <?php 
                                                if($page!=1){
                                                        
                                                        echo '<a href="rent_house_list.php?page='.$previous.'&split'.$url.'"><button class="tombol text-center"><b><</b></button></a>';
                                                }
                                        ?>
                                        <button class="tombol text-center"><?php echo $page ?></button>
                                        <?php 
                                                if($page_total > $page){
                                                        echo '<a href="rent_house_list.php?page='.$next.'&split'.$url.'"><button class="tombol text-center"><b>></b></button></a>';
                                                }
                                        ?>
                                </div>
                        <?php } ?>   
                    </div>
                    <div class="col-sm-3 kotak-kanan">
                            <p class="text-center tulisan2">FILTER</p>
                            <div class="text-center">
                            <input type="text" name="name" id="name" placeholder="&nbsp; &nbsp; NAME" class="img-fluid filter">
                            <input type="text" name="price" id="price" placeholder="&nbsp; &nbsp; PRICE" class="img-fluid filter">
                            <input type="text" name="location" id="location" placeholder="&nbsp; &nbsp; LOCATION" class="img-fluid filter">
                            <br>
                            <?php if(isset($_GET['name'])){
                                echo '<button class="search text-center" onclick="search_()">Search</button>';
                            }else{
                                echo '<button class="search text-center" onclick="search()">Search</button>';
                            }
                            ?>
                        
                        </div>
                        
                        
                        
                        </div>
            </div>
        </div>
     

       



      

            <!-- footer -->
            <footer class="page-footer font-small pt-4">

                    <!-- Footer Links -->
                    <div class="container-fluid text-center text-md-left">
                  
                      <!-- Grid row -->
                      <div class="row">
                  
                        <!-- Grid column -->
                        <div class="col-sm-4 tulisan">
                  
                          <!-- Content -->
                  
                        </div>
                        <!-- Grid column -->
                  
                        <!-- Grid column -->
                        <div class="col-sm-4 tulisan">
                  
                            <!-- Links -->
                            <h6 class="text-uppercase footer-caption">Quick Links</h6>
                  
                            <ul class="list-unstyled">
                                <li>
                                        <a class="footercaption" href="index.php">HOME</a>
                                </li>
                                <li>
                                        <a class="footercaption" href="rent_house_list.php">RENT</a>
                                </li>
                                <li>
                                        <a class="footercaption" href="about_us.php">ABOUT US</a>
                                </li>
                        </ul>
                            
                            <!-- Copyright -->
                            <div class="footer-copyright text-center py-3 tulisan">© 2019 Copyright:
                                <a class="footercaption" href="index.php"> SewaRumah.com</a>
                            </div>
                          <!-- Copyright -->
                          </div>
                          <!-- Grid column -->
                  
                          <!-- Grid column -->
                          <div class="col-sm-4 tulisan">
                  
                            <!-- Links -->
                            <h5 class="text-uppercase tulisan">contact us</h5>
                  
                            <ul class="list-unstyled">
                                <img class="imagefooter" src="img/aset/facebook.png" alt="">
                                <img src="img/aset/instagram.png" alt="" class="imagefooter">
                                <img src="img/aset/twitter.png" alt="" class="imagefooter">
                                <img src="img/aset/email.png" alt="" class="imagefooter">
                            </ul>
                  
                          </div>
                          <!-- Grid column -->
                  
                      </div>
                      <!-- Grid row -->

                    </div>
                    <!-- Footer Links -->
                  </footer>
                  <!-- footer end -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->   
  </body>
</html>  